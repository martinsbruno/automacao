/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interagese.restaurante.converter;


import br.com.interagese.restaurante.bean.producer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author adam
 */
public class ConverterUtil {

    public ConverterUtil() {
    }

    protected producer getBean() {
        try {
            Context c = new InitialContext();
            return (producer) c.lookup("java:global/restaurante/UtilsBean!br.com.bruno.bean.UtilsBean");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}
