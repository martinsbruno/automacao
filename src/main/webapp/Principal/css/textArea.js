function cursorPosition() {
    var textarea = document.getElementById("geral:tab:editor_input");
    textarea.focus();

    // get selection in firefox, opera, ...

    if (typeof (textarea.selectionStart) == 'number')

    {
        alert(textarea.selectionStart);

    } else if (document.selection) {
        var selection_range = document.selection.createRange().duplicate();

        if (selection_range.parentElement() == textarea) {    // Check that the selection is actually in our textarea
            // Create three ranges, one containing all the text before the selection,
            // one containing all the text in the selection (this already exists), and one containing all
            // the text after the selection.
            var before_range = document.body.createTextRange();
            before_range.moveToElementText(textarea);                    // Selects all the text
            before_range.setEndPoint("EndToStart", selection_range);     // Moves the end where we need it

            var after_range = document.body.createTextRange();
            after_range.moveToElementText(textarea);                     // Selects all the text
            after_range.setEndPoint("StartToEnd", selection_range);      // Moves the start where we need it

            var before_finished = false, selection_finished = false, after_finished = false;
            var before_text, untrimmed_before_text, selection_text, untrimmed_selection_text, after_text, untrimmed_after_text;

            // Load the text values we need to compare
            before_text = untrimmed_before_text = before_range.text;
            selection_text = untrimmed_selection_text = selection_range.text;
            after_text = untrimmed_after_text = after_range.text;

            // Check each range for trimmed newlines by shrinking the range by 1 character and seeing
            // if the text property has changed.  If it has not changed then we know that IE has trimmed
            // a \r\n from the end.
            do {
                if (!before_finished) {
                    if (before_range.compareEndPoints("StartToEnd", before_range) == 0) {
                        before_finished = true;
                    } else {
                        before_range.moveEnd("character", -1)
                        if (before_range.text == before_text) {
                            untrimmed_before_text += "\r\n";
                        } else {
                            before_finished = true;
                        }
                    }
                }
                if (!selection_finished) {
                    if (selection_range.compareEndPoints("StartToEnd", selection_range) == 0) {
                        selection_finished = true;
                    } else {
                        selection_range.moveEnd("character", -1)
                        if (selection_range.text == selection_text) {
                            untrimmed_selection_text += "\r\n";
                        } else {
                            selection_finished = true;
                        }
                    }
                }
                if (!after_finished) {
                    if (after_range.compareEndPoints("StartToEnd", after_range) == 0) {
                        after_finished = true;
                    } else {
                        after_range.moveEnd("character", -1)
                        if (after_range.text == after_text) {
                            untrimmed_after_text += "\r\n";
                        } else {
                            after_finished = true;
                        }
                    }
                }

            } while ((!before_finished || !selection_finished || !after_finished));

            // Untrimmed success test to make sure our results match what is actually in the textarea
            // This can be removed once you're confident it's working correctly
            var untrimmed_text = untrimmed_before_text + untrimmed_selection_text + untrimmed_after_text;
            var untrimmed_successful = false;
            if (textarea.value == untrimmed_text) {
                untrimmed_successful = true;
            }
            // ** END Untrimmed success test

            var startPoint = untrimmed_before_text.length;
            alert(startPoint);

        }
    }
}

/*
 Editor de texto criado por Jonathan Coutinho Luz de Queiroz
 Usu?rio: http://scriptbrasil.com.br/forum/index.php?showuser=37857
 T?pico: http://scriptbrasil.com.br/forum/index.php?showtopic=138499
 */
function getSelection(inputBox) {
    /*Se possuir as propriedades selectionStart e selectionEnd, retorna
     o valor das mesmas
     */
    if ("selectionStart" in inputBox) {
        return{
            start: inputBox.selectionStart,
            end: inputBox.selectionEnd
        }
    }
    //Se n?o, retorna "false"
    return false
}
function InsereCodigo(valor, tag_fechamento, tipo, minvalue, maxvalue) {
    var textarea = document.getElementById("geral:tab:editor_input")
    var t = getSelection(textarea)
    var valor2 = ""
    if (tipo != null) {
        valor2 = "="
        var vn
        do {
            vn = prompt(tipo)
        } while ((minvalue && maxvalue) && (vn > maxvalue || vn < minvalue))
        valor2 += vn
    }
    //Navegadores sem suporte a selectionStart ou selecionEnd
    if (t == false) {
        //IE
        textarea.focus()
        var y = document.selection.createRange().text
        y = "[" + valor + valor2 + "]" + y + (tag_fechamento ? "[/" + valor + "]" : "")
        document.selection.createRange().text = y
        return
    }
    //Outros navegadores
    var valor_inicial = textarea.value
    var tamanho_selecionado = t.end - t.start
    var inicio_selecao = t.start
    var fim_selecao = (tag_fechamento ? t.end : valor_inicial.length)
    textarea.value = ""
    textarea.value = valor_inicial.substring(0, inicio_selecao)
    textarea.value += "[" + valor + valor2 + "]"
    textarea.value += valor_inicial.substring(inicio_selecao, fim_selecao)
    if (tag_fechamento) {
        textarea.value += "[/" + valor + "]"
        textarea.value += valor_inicial.substring(fim_selecao, valor_inicial.length)
    }
    //d? novamente o foco ao textarea
    textarea.focus()
    textarea.selectionStart = inicio_selecao + valor.length + valor2.length + 2
    textarea.selectionEnd = textarea.selectionStart + tamanho_selecionado
}
onload = function () {
    //Negrito
    CreateTag("geral:InsereNegrito", "b", true)
    //It?lico
    CreateTag("InsereItalico", "i", true)
    //Sublinhado
    CreateTag("InsereSublinhado", "u", true)
    //Riscado
    CreateTag("InsereRiscado", "s", true)
    //Overline
    CreateTag("InsereOverline", "overline", true)
    //Sobrescrito
    CreateTag("InsereSobrescrito", "sobrescrito", true)
    //Subscrito
    CreateTag("InsereSubscrito", "subscrito", true)
    //Pr?-formata??o
    CreateTag("InserePre", "pre", true)
    //Inserir imagem
    CreateTag("InsereImagem", "img", false, "Digite uma imagem")
    //Inserir usu?rio
    CreateTag("InsereUser", true, "Digite o nome do usu?rio")
    //Inserir e-mail
    CreateTag("InsereMail", true, "Digite o e-mail")
    //Modificar tamanho
    CreateTag("InsereSize", "size", true, "Tamanho da letra (1 a 7)", 1, 7)
    //Link's
    CreateTag("InsereURL", "url", true, "Digite a p?gina desejada")
    //Coment?rios
    CreateTag("InsereComentario", "comment", true)
    //Lista
    CreateTag("InsereLista", "list", true)
    //Marcador
    CreateTag("InsereMarcador", "*", false)
    //Tabela
    CreateTag("InsereTabela", "table", true)
    //Thead
    CreateTag("InsereTHead", "thead", true)
    //Tbody
    CreateTag("InsereTBody", "tbody", true)
    //Linha
    CreateTag("InsereLinha", "tr", true)
    //Coluna
    CreateTag("InsereColuna", "td", true)
    //Quote
    CreateTag("InsereQuote", "quote", true)
    //Code
    CreateTag("InsereCode", "code", true)
    //ASP
    CreateTag("InsereASP", "asp", true)
}
function CreateTag(id, v1, v2, v3, v4, v5) {
    var el = document.getElementById(id)
    if (el) {
        el.onclick = function () {
            InsereCodigo(v1, v2, v3, v4, v5)
        }
    }
}
/*
 ---- Como adcionar tag's ----
 Para adcionar tag's ao editor, utilize a fun??o CreateTag.
 Apenas os dois primeiros par?metros s?o obrigat?rios.
 1. O primeiro par?metro da fun??o indica o id do elemento HTML que
 ser? utilizado pelo usu?rio para inserir a tag.
 2. O segundo par?metro indica o nome da tag (ex.: "quote",
 "table", etc.)
 3. O terceiro par?metro indica se a tag dever? ser fechada
 4. Caso haja um quato par?metro, o usu?rio ser? indagado
 sobre o valor da tag (c?digo ap?s o "=")
 4.1 Se n?o houver esse par?metro, subentende-se que a tag
 n?o necessita de nenhum valor
 5. O quinto e sexto par?metro indica o valor m?ximo
 que ser? aceito na pergunta do quarto par?metro
 5.1 Se n?o houverem esses par?metros, ser? aceito qualquer
 valor (inclusive texto)
 */

function getPosCursor(element) {
    var value = 0;
    if (typeof (element.selectionStart) != "undefined") {
        value = element.selectionStart;
    } else if (document.selection) {
        var range = document.selection.createRange();
        var storedRange = range.duplicate();
        storedRange.moveToElementText(element);
        storedRange.setEndPoint("EndToEnd", range);
        value = storedRange.text.length - range.text.length;
    }
    return value;
}

var posicaoAtual;

function atualizaPosicao() {
    obj = document.getElementById("geral:tab:editor_input");
    posicaoAtual = getPosCursor(obj);
}

function insereTexto() {
    //Pega a textarea
    var textarea = document.getElementById("geral:tab:editor_input");

    //Texto a ser inserido
    var texto = document.getElementById("geral:tab:teste").value;

    //inicio da sele??o
    var sel_start = posicaoAtual

    //final da sele??o
    var sel_end = sel_start;


    if (!isNaN(textarea.selectionStart))
            //tratamento para Mozilla
            {
                var sel_start = textarea.selectionStart;
                var sel_end = textarea.selectionEnd;

                mozWrap(textarea, texto, '')
                textarea.selectionStart = sel_start + texto.length;
                textarea.selectionEnd = sel_end + texto.length;
            } else if (textarea.createTextRange && textarea.caretPos)
    {
        if (baseHeight != textarea.caretPos.boundingHeight)
        {
            textarea.focus();
            storeCaret(textarea);
        }
        var caret_pos = textarea.caretPos;
        caret_pos.text = caret_pos.texto.charAt(caret_pos.texto.length - 1) == ' ' ? caret_pos.text + text + ' ' : caret_pos.text + text;

    } else //Para quem n?o ? poss?vel inserir, inserimos no final mesmo (IE...)
    {
        textarea.value = textarea.value + texto;
    }
}

/*
 Essa fun??o abre o texto em duas strings e insere o texto bem na posi??o do cursor, ap?s ele une novamento o texto mas com o texto inserido
 Essa maravilhosa fun??o s? funciona no Mozilla... No IE n?o temos as propriedades selectionstart, textLength...
 */
function mozWrap(txtarea, open, close)
{
    var selLength = txtarea.textLength;
    var selStart = txtarea.selectionStart;
    var selEnd = txtarea.selectionEnd;
    var scrollTop = txtarea.scrollTop;

    if (selEnd == 1 || selEnd == 2)
    {
        selEnd = selLength;
    }
    //S1 tem o texto do come?o at? a posi??o do cursor
    var s1 = (txtarea.value).substring(0, selStart);

    //S2 tem o texto selecionado
    var s2 = (txtarea.value).substring(selStart, selEnd)

    //S3 tem todo o texto selecionado
    var s3 = (txtarea.value).substring(selEnd, selLength);

    //COloca o texto na textarea. Utiliza a string que estava no in?cio, no meio a string de entrada, depois a sele??o seguida da string
    //de fechamento e por fim o que sobrou ap?s a sele??o
    txtarea.value = s1 + open + s2 + close + s3;
    txtarea.selectionStart = selEnd + open.length + close.length;
    txtarea.selectionEnd = txtarea.selectionStart;
    txtarea.focus();
    txtarea.scrollTop = scrollTop;
    return;
}
/*
 Insert at Caret position. Code from
 [url]http://www.faqts.com/knowledge_base/view.phtml/aid/1052/fid/130[/url]
 */
function storeCaret(textEl)
{
    if (textEl.createTextRange)
    {
        textEl.caretPos = document.selection.createRange().duplicate();
    }
}  