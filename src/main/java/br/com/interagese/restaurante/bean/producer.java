/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interagese.restaurante.bean;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

/**
 *
 * @author bruno
 */
public class producer {

    @PersistenceUnit(name = "restaurantePU")
    private EntityManagerFactory emf;

    @Produces
    @RequestScoped
    public EntityManager createEntityManager() {
        System.out.println("Cria");
        return emf.createEntityManager();
    }

    public void closeEntityManager(@Disposes EntityManager em) {
        System.out.println("Fecha");
        if (em.isOpen()) {
            em.close();
        }
    }

}
