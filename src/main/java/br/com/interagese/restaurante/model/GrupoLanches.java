/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interagese.restaurante.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author bruno
 */
public class GrupoLanches {

    private Integer id;
    private String nmGrupo;
    private List<Mesas> resultLanches;
    private Integer active;

    public GrupoLanches(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNmGrupo() {
        nmGrupo = "Grupo " + id;
        return nmGrupo;
    }

    public List<Mesas> getResultLanches() {
        if (resultLanches == null) {
            resultLanches = new ArrayList<>();
        }
        return resultLanches;
    }

    public void setResultLanches(List<Mesas> result) {
        this.resultLanches = result;
    }

    /**
     * @return the active
     */
    public Integer getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Integer active) {
        this.active = active;
    }

}
