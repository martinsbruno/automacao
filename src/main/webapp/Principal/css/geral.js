var win = null;
var mensagemGeral;
var objGerarFoco;



function validadorCampoObrigatorio(obj, mensagem) {
    /**if (obj.value.trim() === "") {
     objGerarFoco = obj;
     mensagemGeral = mensagem;
     chamarDialogMensagem();
     }**/
}

function inputSomentesValoresReal(input){
    valor = input.value;
    tamanho = valor.length;
    novoValor = "";

    posicaoVirgula = valor.indexOf(",");
    if (posicaoVirgula >= 0) {
        tamanho = posicaoVirgula + 3;
    }




    for (i = 0; i < tamanho; i++) {

        if (valor.substr(i, 1) === "0" ||
                valor.substr(i, 1) === "1" ||
                valor.substr(i, 1) === "2" ||
                valor.substr(i, 1) === "3" ||
                valor.substr(i, 1) === "4" ||
                valor.substr(i, 1) === "5" ||
                valor.substr(i, 1) === "6" ||
                valor.substr(i, 1) === "7" ||
                valor.substr(i, 1) === "8" ||
                valor.substr(i, 1) === "9" ||
                valor.substr(i, 1) === ",") {

            novoValor = novoValor + valor.substr(i, 1);
        }
    }

    input.value = novoValor;

}

function inputSomentesValoresReal2(input, casas) {
    valor = input.value;
    tamanho = valor.length;
    novoValor = "";

    posicaoVirgula = valor.indexOf(",");
    if (posicaoVirgula >= 0) {
        tamanho = posicaoVirgula + (casas + 1);
    }




    for (i = 0; i < tamanho; i++) {

        if (valor.substr(i, 1) === "0" ||
                valor.substr(i, 1) === "1" ||
                valor.substr(i, 1) === "2" ||
                valor.substr(i, 1) === "3" ||
                valor.substr(i, 1) === "4" ||
                valor.substr(i, 1) === "5" ||
                valor.substr(i, 1) === "6" ||
                valor.substr(i, 1) === "7" ||
                valor.substr(i, 1) === "8" ||
                valor.substr(i, 1) === "9" ||
                valor.substr(i, 1) === ",") {

            novoValor = novoValor + valor.substr(i, 1);
        }
    }

    input.value = novoValor;

}

function inputValorRealOnBlur(input) {

    valor = input.value;

    if (valor == "")
        return;

    posicaoVirgula = valor.indexOf(",");
    if (posicaoVirgula < 0) {
        valor = valor + ",00";
    } else {
        decimaisTemp = valor.substr(posicaoVirgula + 1, valor.length - posicaoVirgula - 1);
        decimais = zeroDireita(decimaisTemp, 2);
        valor = valor.replace("," + decimaisTemp, "");
        valor = valor + "," + decimais;
    }

    input.value = valor;
}

function inputValorRealAndZeroOnBlur(input) {

    valor = input.value;

    if (valor === "") {
        valor = 0;
        valor = valor + ",00";
        input.value = valor;
        return;
    }


    posicaoVirgula = valor.indexOf(",");
    if (posicaoVirgula < 0) {
        valor = valor + ",00";
    } else {
        decimaisTemp = valor.substr(posicaoVirgula + 1, valor.length - posicaoVirgula - 1);
        //decimais = zeroDireita(decimaisTemp, 2);
        decimais = decimaisTemp;
        valor = valor.replace("," + decimaisTemp, "");
        valor = valor + "," + decimais;
    }

    input.value = valor;
}

function inputSomentesValoresRealKeyUp(input) {
    valor = input.value;
    tamanho = valor.length;
    novoValor = "";

    posicaoVirgula = valor.indexOf(",");
    if (posicaoVirgula >= 0) {
        tamanho = posicaoVirgula + 3;
    }


    for (i = 0; i < tamanho; i++) {

        if (valor.substr(i, 1) == "0" ||
                valor.substr(i, 1) == "1" ||
                valor.substr(i, 1) == "2" ||
                valor.substr(i, 1) == "3" ||
                valor.substr(i, 1) == "4" ||
                valor.substr(i, 1) == "5" ||
                valor.substr(i, 1) == "6" ||
                valor.substr(i, 1) == "7" ||
                valor.substr(i, 1) == "8" ||
                valor.substr(i, 1) == "9" ||
                valor.substr(i, 1) == ",") {

            novoValor = novoValor + valor.substr(i, 1);
        }
    }

    input.value = novoValor;

}

function validaHora(input) {
    horasV = (input.value.substring(0, 2));
    minutosV = (input.value.substring(3, 5));


    if (input.value == "__:__" || input.value == "") {
        return true;
    }


    estadoV = "";
    if ((horasV < 00) || (horasV > 23) || (minutosV < 00) || (minutosV > 59)) {
        estadoV = "errada";
    }

    if (input.value == "") {
        estadoV = "errada";
    }
    if (estadoV == "errada") {
        alert("Hora invalida!");
        input.value = "";
        input.focus();
    }
}

function validaData(input)
{
    var bissexto = 0;
    var data = input.value;
    var tam = data.length;

    if (input.value == "__/__/____" || input.value == "") {
        return true;
    }

    if (tam == 10)
    {
        var dia = data.substr(0, 2)
        var mes = data.substr(3, 2)
        var ano = data.substr(6, 4)
        if ((ano > 1900) || (ano < 2100))
        {
            switch (mes)
            {
                case '01':
                case '03':
                case '05':
                case '07':
                case '08':
                case '10':
                case '12':
                    if (dia <= 31)
                    {
                        return true;
                    }
                    break

                case '04':
                case '06':
                case '09':
                case '11':
                    if (dia <= 30)
                    {
                        return true;
                    }
                    break
                case '02':
                    /* Validando ano Bissexto / fevereiro / dia */
                    if ((ano % 4 == 0) || (ano % 100 == 0) || (ano % 400 == 0))
                    {
                        bissexto = 1;
                    }
                    if ((bissexto == 1) && (dia <= 29))
                    {
                        return true;
                    }
                    if ((bissexto != 1) && (dia <= 28))
                    {
                        return true;
                    }
                    break
            }
        }
    }
    input.value = "";
    input.focus();
    alert("A data " + data + " inválida!");

    return true;
}

// Popup window code
function newPopup(url) {
    w = screen.width;
    h = screen.height;


    //divide a resolu��o por 2, obtendo o centro do monitor
    meio_w = w / 2;
    meio_h = h / 2;
    altura = 539;
    largura = 765;

    //diminui o valor da metade da resolu��o pelo tamanho da janela, fazendo com q ela fique centralizada
    altura2 = altura / 2;
    largura2 = largura / 2;
    meio1 = meio_h - altura2;
    meio2 = meio_w - largura2;

    //abre a nova janela, j� com a sua devida posi��o

    popupWindow = window.showModalDialog(
            //pega a resolu��o do visitante


            url, 'popUpWindow', 'resizable: false;dialogWidth:765px; dialogHeight:539px; dialogTop:' + meio1 + ';dialogLeft:' + meio2 + ' ')
}


function setarsalvar() {
    var obj = document.getElementById('geral:canal');
    obj.focus();
}

function iniciarlocate(objeto, event) {
    if (event.keyCode === 113)
        document.getElementById(objeto).click();
}

function setarfoco(objeto) {
    var obj = document.getElementById(objeto);
    if (obj !== null)
        obj.focus();
}



function check(obj, master) {

    for (var i = 0; i < document.formPadrao.elements.length; i++) {

        var x = document.formPadrao.elements[i];

        if (x.name.toString().indexOf(obj) > -1) {
            x.checked = master.checked;
        }

    }

}

function refresh() {
    window.location.reload();
}

function PopUp() {
    Width = 800;
    Height = 600;
    if (win && win.open)
        win.close()
    win = window.open("../ReportTemp", "Eurus tecnologia", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,border=0,resizable=yes,width=" + Width + ",height=" + Height + ",copyhistory=no,top=120");
}



function abrirJanela(url) {
    window.open(url, 'page', 'toolbar=no,left=0,top=0,location=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=400,height=300');
}

function grid(event) {
    var dataGrid = f_core.GetElementById("directoryGrid");
    dataGrid.f_setFocus();
    dataGrid.f_refreshContent();

}

function cidade(event) {
    var dataGrid = f_core.GetElementById("grid");
    dataGrid.f_setFocus();
    dataGrid.f_refreshContent();

}

function atualizaM() {
    var tj = document.getElementsByName('forminc:tipoJuros');
    var ij = document.getElementById('forminc:juros');


    if (tj[0].checked)
        ij.disabled = true;
    else
        ij.disabled = false;



}

function testButton() {

    var edit = document.getElementById('forminc:nrchequefinal');
    var t = document.getElementsByName('forminc:tipo');

    if (t[0].checked) {
        edit.disabled = true;
        edit.value = '';
    } else
        edit.disabled = false;
}




function testtrib() {

    var aliquota = document.getElementsByName('forminc:aliquota');
    var ufm = document.getElementsByName('forminc:ufm');


    if (aliquota[0].checked) {

        ufm[0].disabled = true;
        ufm[1].disabled = true;
    } else {
        ufm[0].disabled = false;
        ufm[1].disabled = false;
        ufm[0].checked = true;
    }


}

documentall = document.all;
/*
 * fun��o para formata��o de valores monet�rios retirada de
 * http://jonasgalvez.com/br/blog/2003-08/egocentrismo
 */

function formatamoney(c) {
    var t = this;
    if (c == undefined)
        c = 2;
    var p, d = (t = t.split("."))[1].substr(0, c);
    for (p = (t = t[0]).length; (p -= 3) >= 1; ) {
        t = t.substr(0, p) + "." + t.substr(p);
    }
    return t + "," + d + Array(c + 1 - d.length).join(0);
}

String.prototype.formatCurrency = formatamoney

function demaskvalue(valor, currency) {
    /*
     * Se currency � false, retorna o valor sem apenas com os n�meros. Se � true, os dois �ltimos caracteres s�o considerados as 
     * casas decimais
     */
    var val2 = '';
    var strCheck = '0123456789';
    var len = valor.length;
    if (len == 0) {
        return 0.00;
    }

    if (currency == true) {
        /* Elimina os zeros � esquerda 
         * a vari�vel  <i> passa a ser a localiza��o do primeiro caractere ap�s os zeros e 
         * val2 cont�m os caracteres (descontando os zeros � esquerda)
         */

        for (var i = 0; i < len; i++)
            if ((valor.charAt(i) != '0') && (valor.charAt(i) != ','))
                break;

        for (; i < len; i++) {
            if (strCheck.indexOf(valor.charAt(i)) != -1)
                val2 += valor.charAt(i);
        }

        if (val2.length == 0)
            return "0.00";
        if (val2.length == 1)
            return "0.0" + val2;
        if (val2.length == 2)
            return "0." + val2;

        var parte1 = val2.substring(0, val2.length - 2);
        var parte2 = val2.substring(val2.length - 2);
        var returnvalue = parte1 + "." + parte2;
        return returnvalue;

    } else {
        /* currency � false: retornamos os valores COM os zeros � esquerda, 
         * sem considerar os �ltimos 2 algarismos como casas decimais 
         */
        val3 = "";
        for (var k = 0; k < len; k++) {
            if (strCheck.indexOf(valor.charAt(k)) != -1)
                val3 += valor.charAt(k);
        }
        return val3;
    }
}




function backspace(obj, event) {
    /*
     Essa fun��o basicamente altera o  backspace nos input com m�scara reais para os navegadores IE e opera.
     O IE n�o detecta o keycode 8 no evento keypress, por isso, tratamos no keydown.
     Como o opera suporta o infame document.all, tratamos dele na mesma parte do c�digo.
     */

    var whichCode = (window.Event) ? event.which : event.keyCode;
    if (whichCode == 8 && documentall) {
        var valor = obj.value;
        var x = valor.substring(0, valor.length - 1);
        var y = demaskvalue(x, true).formatCurrency();

        obj.value = ""; //necess�rio para o opera
        obj.value += y;

        if (event.preventDefault) { //standart browsers
            event.preventDefault();
        } else { // internet explorer
            event.returnValue = false;
        }
        return false;

    }// end if		
}// end backspace






function foco(campo) {
    i = document.getElementById('forminc:' + campo);
    //globalvar = campo;
    i.focus();
//setTimeout("globalvar.focus()",50);
}
function Verificar() {
    var tecla = window.event.keyCode;
    if (tecla == 116) {
        event.keyCode = 0;
        event.returnValue = false;
    }
}


function mascara(o, f) {
    v_obj = o
    v_fun = f
    setTimeout("execmascara()", 1)
}

function execmascara() {
    v_obj.value = v_fun(v_obj.value)
}

function leech(v) {
    v = v.replace(/o/gi, "0")
    v = v.replace(/i/gi, "1")
    v = v.replace(/z/gi, "2")
    v = v.replace(/e/gi, "3")
    v = v.replace(/a/gi, "4")
    v = v.replace(/s/gi, "5")
    v = v.replace(/t/gi, "7")
    return v
}

function soNumeros(v) {
    return v.replace(/\D/g, "")
}

function telefone(v) {
    v = v.replace(/\D/g, "")                 //Remove tudo o que n�o � d�gito
    v = v.replace(/^(\d\d)(\d)/g, "($1) $2") //Coloca par�nteses em volta dos dois primeiros d�gitos
    v = v.replace(/(\d{4})(\d)/, "$1-$2")    //Coloca h�fen entre o quarto e o quinto d�gitos
    return v
}
function rg(v) {
    v = v.replace(/D/g, "")                //Remove tudo o que n�o � d�gito
    v = v.replace(/^(\d{3})(\d)/, "$1.$2") //Esse � t�o f�cil que n�o merece explica��es
    return v
}

function tributo(v) {
    v = v.replace(/D/g, "")                //Remove tudo o que n�o � d�gito
    return v
}

function cpf(v) {
    v = v.replace(/\D/g, "")                    //Remove tudo o que n�o � d�gito
    v = v.replace(/(\d{3})(\d)/, "$1.$2")       //Coloca um ponto entre o terceiro e o quarto d�gitos
    v = v.replace(/(\d{3})(\d)/, "$1.$2")       //Coloca um ponto entre o terceiro e o quarto d�gitos
    //de novo (para o segundo bloco de n�meros)
    v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2") //Coloca um h�fen entre o terceiro e o quarto d�gitos
    return v
}

function cep(v) {
    v = v.replace(/D/g, "")                //Remove tudo o que n�o � d�gito
    v = v.replace(/^(\d{5})(\d)/, "$1-$2") //Esse � t�o f�cil que n�o merece explica��es
    return v
}

function cnpj(v) {
    v = v.replace(/\D/g, "")                           //Remove tudo o que n�o � d�gito
    v = v.replace(/^(\d{2})(\d)/, "$1.$2")             //Coloca ponto entre o segundo e o terceiro d�gitos
    v = v.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3") //Coloca ponto entre o quinto e o sexto d�gitos
    v = v.replace(/\.(\d{3})(\d)/, ".$1/$2")           //Coloca uma barra entre o oitavo e o nono d�gitos
    v = v.replace(/(\d{4})(\d)/, "$1-$2")              //Coloca um h�fen depois do bloco de quatro d�gitos
    return v
}

function romanos(v) {
    v = v.toUpperCase()             //Mai�sculas
    v = v.replace(/[^IVXLCDM]/g, "") //Remove tudo o que n�o for I, V, X, L, C, D ou M
    //Essa � complicada! Copiei daqui: http://www.diveintopython.org/refactoring/refactoring.html
    while (v.replace(/^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$/, "") != "")
        v = v.replace(/.$/, "")
    return v
}



function formatar(src, mask) {


    var i = src.value.length;
    var saida = mask.substring(0, 1);
    var texto = mask.substring(i)
    if (texto.substring(0, 1) != saida)
    {
        src.value += texto.substring(0, 1);
    }
}


function mascaraData(campoData) {
    //var data = campoData.value;

    formatar(campoData, "##/##/####");
    /*/
     if (data.length >= 10){
     return true;
     }
     
     
     
     if (data.length == 2){
     data = data + '/';
     campoData.value = data;
     
     return true;              
     }
     if (data.length == 5){
     data = data + '/';
     campoData.value = data;
     return true;
     }*/
    return true;
}

function site(v) {
    //Esse sem comentarios para que voc� entenda sozinho ;-)
    v = v.replace(/^http:\/\/?/, "")
    dominio = v
    caminho = ""
    if (v.indexOf("/") > -1)
        dominio = v.split("/")[0]
    caminho = v.replace(/[^\/]*/, "")
    dominio = dominio.replace(/[^\w\.\+-:@]/g, "")
    caminho = caminho.replace(/[^\w\d\+-@:\?&=%\(\)\.]/g, "")
    caminho = caminho.replace(/([\?&])=/, "$1")
    if (caminho != "")
        dominio = dominio.replace(/\.+$/, "")
    v = "http://" + dominio + caminho
    return v
}

function MascaraCEP(campo, teclaPress) {

    if (window.event) {
        var tecla = teclaPress.keyCode;

    } else {
        tecla = teclaPress.which;
    }

    if (tecla == 8) {
        return true;
    }

    var s = new String(campo.value);
    s = s.replace(/(\.|\(|\)|\/|\-| )+/g, '');
    tam = s.length + 1;
    if (tam > 5 && tam < 7)
        campo.value = s.substr(0, 5) + '-' + s.substr(5, tam);
}

function digitos(event) {
    if (key == 8) {
        return true;
    }
    if (window.event) {
        // IE
        key = event.keyCode;
    } else if (event.which) {
        // netscape
        key = event.which;
    }
    if (key != 8 || key != 13 || key < 48 || key > 57)
        return (((key > 47) && (key < 58)) || (key == 8) || (key ==
                13));
    return true;
}
function SomenteNumero(e) {
    var tecla = (window.event) ? event.keyCode : e.which;
    if ((tecla > 47 && tecla < 58))
        return true;
    else {
        if (tecla == 8 || tecla == 0)
            return true;
        else
            return false;
    }
}


function toggleFullScreen() {
    if (!document.fullscreenElement && // alternative standard method
            !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement) {  // current working methods
        if (document.documentElement.requestFullscreen) {
            document.documentElement.requestFullscreen();
        } else if (document.documentElement.msRequestFullscreen) {
            document.documentElement.msRequestFullscreen();
        } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
        } else if (document.documentElement.webkitRequestFullscreen) {
            document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        }
    } else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }
    }
}