/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interagese.restaurante.view;

import br.com.interagese.restaurante.bean.SecaoBean;
import br.com.interagese.restaurante.converter.Contexto;
import br.com.interagese.restaurante.model.Secao;
import br.com.interagese.restaurante.utils.InterView;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author bruno
 */
@ManagedBean
@ViewScoped
public class SecaoView extends InterView {

    @Inject
    private SecaoBean secaoBean;
    private Secao secao;
    private boolean cadasterSecao;
    private List<Secao> secoes;

    @Override
    public String getTitulo() {
        iniciar();
        return ""; //To change body of generated methods, choose Tools | Templates.
    }

    public void iniciar() {
        Contexto.getSessao().setAttribute("INDEX", 7);
        carregarSecao();
    }

    public void carregarSecao() {
        secoes = secaoBean.findByQueries("SELECT o FROM Secao o");
        cadasterSecao = false;
        Contexto.getSessao().setAttribute("CADASTROSECAO", false);
    }

    //****************************** get && setts ******************************
    public SecaoBean getSecaoBean() {
        return secaoBean;
    }

    public void setSecaoBean(SecaoBean secaoBean) {
        this.secaoBean = secaoBean;
    }

    public Secao getSecao() {
        return secao;
    }

    public void setSecao(Secao secao) {
        this.secao = secao;
    }

    public boolean isCadasterSecao() {
        cadasterSecao = Contexto.getSessao().getAttribute("CADASTROSECAO") == null ? false : (boolean) Contexto.getSessao().getAttribute("CADASTROSECAO");
        return cadasterSecao;
    }

    public void setCadasterSecao(boolean cadasterSecao) {
        this.cadasterSecao = cadasterSecao;
    }

    public List<Secao> getSecoes() {
        if (secoes == null) {
            secoes = new ArrayList<>();
        }
        return secoes;
    }

    public void setSecoes(List<Secao> secoes) {
        this.secoes = secoes;
    }

}
