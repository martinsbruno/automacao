/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interagese.restaurante.utils;

import br.com.interagese.restaurante.bean.PadraoBean;
import br.com.interagese.restaurante.converter.Contexto;
import br.com.interagese.exception.InterException;
import br.com.interagese.restaurante.model.Tabusu;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author bruno
 */
@ManagedBean
@ViewScoped
public class InterView {

    public static List<Tabusu> listUsuarioLogado;
    public Tabusu usuarioLogado = (Tabusu) Contexto.getSessao().getAttribute("USUARIO");
    public Integer index = 1;
    public Integer tela;
    public boolean isEditable;
    public PadraoBean bean;
    private boolean cadasterCliente;
    public String titulo = index == 1 ? "Menu Principal" : "";

    public InterView() {

    }

    public void cadastrarCliente() {
        setCadasterCliente(true);
        instancia();
    }

    public void instancia() {

    }

    public void getValidarCampos() throws InterException {

    }

    public Integer getTela() {
        return tela;
    }

    public String getNomeUsuario() {
        return usuarioLogado.getNome();
    }

    public String getSaudacao() {
        String result = "Em, " + getDataLogado() + " "
                + "        " + " - Seja bem-vindo,  " + getNomeUsuario().toUpperCase();
        return result;
    }

    public String getDataLogado() {
        return new SimpleDateFormat("dd/MM/yyyy").format(new Date());
    }

    public Tabusu getUsuarioLogado() {
        usuarioLogado = (Tabusu) Contexto.getSessao().getAttribute("USUARIO");
        return usuarioLogado;
    }

    public Integer getIndex() {
        // index = (Integer) Contexto.getSessao().getAttribute("INDEX");
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public boolean isEditable() {
        return isEditable;
    }

    public String getCommandOutput(String command) {
        String output = null;

        Process process = null;
        BufferedReader reader = null;
        InputStreamReader streamReader = null;
        InputStream stream = null;

        try {
            process = Runtime.getRuntime().exec(command);

            stream = process.getInputStream();
            streamReader = new InputStreamReader(stream);
            reader = new BufferedReader(streamReader);

            String currentLine = null;
            StringBuilder commandOutput = new StringBuilder();
            while ((currentLine = reader.readLine()) != null) {
                commandOutput.append(currentLine);
            }

            int returnCode = process.waitFor();
            if (returnCode == 0) {
                output = commandOutput.toString();
            }

        } catch (IOException e) {
            System.err.println(e);
            output = null;
        } catch (InterruptedException e) {
            System.err.println(e);
        } finally {

            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    System.err.println(e.getLocalizedMessage());
                }
            }
            if (streamReader != null) {
                try {
                    streamReader.close();
                } catch (IOException e) {
                    System.err.println(e.getLocalizedMessage());
                }
            }
            if (reader != null) {
                try {
                    streamReader.close();
                } catch (IOException e) {
                    System.err.println(e.getLocalizedMessage());
                }
            }
        }

        return output;
    }

    public void addErrorMessages(String mensagem) {
        FacesMessage msg;
        msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, mensagem, "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void addSuccessMessage(String mensagem) {
        FacesMessage msg;
        msg = new FacesMessage(FacesMessage.SEVERITY_INFO, mensagem, "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void addAlertMessage(String mesangem) {
        FacesMessage msg;
        msg = new FacesMessage(FacesMessage.SEVERITY_WARN, mesangem, "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void exit() {
        if (listUsuarioLogado != null && !listUsuarioLogado.isEmpty()) {
            listUsuarioLogado.remove(getUsuarioLogado());
        }
        Contexto.getSessao().setAttribute("USUARIO", null);
        Contexto.getSessao().setAttribute("INDEX", null);
    }

    public String getTitulo() {
        return titulo;
    }

    public void menu() {
        titulo = "Menu Principal";
        index = 1;
    }

    public void cadastro() {
        Contexto.getSessao().setAttribute("INDEX", 2);
        titulo = "Cadastros";
        index = 2;
    }

    public void cliente() {
        titulo = "Cadastro de Cliente";
        index = 3;
    }

    public void vendas() {
        titulo = "Restaurante";
        index = 8;
    }

    public void usuario() {
        titulo = "Cadastro de Usuário";
        index = 9;
    }

    public void mesas() {
        titulo = "Cadastro de Mesas";
        index = 10;
    }

    /**
     * @return the cadasterCliente
     */
    public boolean isCadasterCliente() {
        return cadasterCliente;
    }

    /**
     * @param cadasterCliente the cadasterCliente to set
     */
    public void setCadasterCliente(boolean cadasterCliente) {
        this.cadasterCliente = cadasterCliente;
    }

}
