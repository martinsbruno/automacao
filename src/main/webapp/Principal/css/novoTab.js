function carregaEnterAsTab() {


    for (j = 0; j < document.forms.length; j++) {

        for (i = 0; i < document.forms[j].elements.length; i++) {

            elem = document.forms[j].elements[i];

            elem.addEventListener('keypress', function (e) {
                autoTab(this, e);
            });
        }
    }
}




function autoTab(input, e) {
    var ind = 0;
    var isNN = (navigator.appName.indexOf("Netscape") !== -1);
    var keyCode = (isNN) ? e.which : e.keyCode;
    var nKeyCode = e.keyCode;
    if (keyCode === 13) {
        if (!isNN) {
            window.event.keyCode = 0;
        } // evitar o beep  


        ind = getIndex(input);

        if (input.tagName !== 'BUTTON' &&
                input.tagName !== 'INPUT' &&
                input.tagName !== 'SELECT') {
            e.preventDefault();
            return;
        }

        /*if (input.form[ind].tagName === 'textarea') {
         e.preventDefault();
         return;
         }*/



        if (input.tagName !== 'BUTTON') {
            e.preventDefault();
        } else {
            return;
        }



        ind = getProximoIndex(ind);

        input.form[ind].focus();
        if (input.form[ind].tagName === 'INPUT' || input.form[ind].tagName === 'SELECT') {
            input.form[ind].select();
        }



    }


    function getProximoIndex(index) {

        if ((index + 1) === input.form.length) {
            index = -1;
        }


        while (index < input.form.length) {
            index++;

            if ((input.form[index].tagName === 'INPUT' && input.form[index].type !== 'hidden') || input.form[index].tagName === 'BUTTON'
                    || input.form[index].tagName === 'SELECT' || input.form[index].tagName === 'TEXTAREA') {
                if (!input.form[index].disabled && !input.form[index].readOnly) {
                    return index;
                }
            }

        }

    }

    function getIndex(input) {
        var index = -1, i = 0, found = false;

        while (i < input.form.length && index === - 1)
            if (input.form[i] === input) {
                index = i;
                if (i < (input.form.length - 1)) {

                    if (input.form[i + 1].disabled || input.form[i + 1].readOnly) {
                        index++;
                    } else {

                        if (input.form[i + 1].type === 'hidden') {
                            index++;
                        }
                    }
                }
            } else {
                i++;
            }


        return index;
    }
}
