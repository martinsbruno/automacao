/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interagese.restaurante.view;

import br.com.interagese.restaurante.bean.ClienteBean;
import br.com.interagese.restaurante.converter.Contexto;
import br.com.interagese.restaurante.converter.Utils;
import br.com.interagese.restaurante.model.Cliente;
import br.com.interagese.restaurante.model.Endereco;
import br.com.interagese.restaurante.model.Pessoa;
import br.com.interagese.restaurante.model.Telefone;
import br.com.interagese.restaurante.utils.InterView;
import br.com.interagese.exception.InterException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import org.primefaces.context.RequestContext;

/**
 *
 * @author bruno
 */
@ManagedBean
@ViewScoped
public class ClienteView extends InterView {

    @Inject
    private ClienteBean clienteBean;
    private Cliente cliente;
    private String nmCliente;
    private Pessoa pessoa;
    private List<Cliente> clientes;
    private List<Telefone> telefones;

    private boolean cadasterTelefone;
    private boolean visible;
    private Endereco endereco;
    private Telefone telefone;

    public void iniciar() {
        cliente();
        carregarCliente();

    }

    public void carregarCliente() {
        clientes = clienteBean.findByQueries("Select o from Cliente o");
        setCadasterCliente(false);
        Contexto.getSessao().setAttribute("CADASTROCLIENTE", false);
    }

    @Override
    public void getValidarCampos() throws InterException {

        if (cliente != null) {
            if (cliente.getNmCliente() == null || cliente.getNmCliente().isEmpty()) {
                throw new InterException("Nome do Cliente não Informado !!");
            }
            if (cliente.getPessoa().getCpfCnpj() == null || cliente.getPessoa().getCpfCnpj().isEmpty()) {
                throw new InterException("Cpf/Cnpj do Cliente não Informado !!");
            }
            if (cliente.getPessoa().getCpfCnpj() != null && !cliente.getPessoa().getCpfCnpj().isEmpty()) {
                boolean resp = Utils.validarCpfCnpj(cliente.getPessoa().getCpfCnpj());
                if (!resp) {
                    throw new InterException("Cpf/Cnpj Inválido !!");
                }
            }
            if (cliente.getPessoa().getListTelefone().isEmpty()) {
                throw new InterException("Telefone do Cliente não Informdo !!");
            }

            if (!isEditable) {
                List<Cliente> c = clienteBean.findByQueries("SELECT o FROM Cliente o WHERE o.id IS NOT NULL and o.pessoa.cpfCnpj='" + cliente.getPessoa().getCpfCnpj() + "'");
                if (!c.isEmpty()) {
                    throw new InterException("Cliente já Cadastrado na base de dados !!");
                }
            }

        } else {
            throw new InterException("Problemas Técnicos, feche a aplicação e abra novamente !!");
        }
        super.getValidarCampos();
    }

    public void incluirCliente() {
        try {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("sucesso", false);
            getValidarCampos();
            if (cliente.getId() == null) {
                clienteBean.insert(cliente);
                addSuccessMessage("Cliente Adicionado com sucesso !!");
            } else {
                clienteBean.update(cliente);
                addSuccessMessage("Cliente Atualizado com sucesso !!");
            }
            context.addCallbackParam("sucesso", true);
            carregarCliente();
        } catch (InterException ie) {
            addErrorMessages(ie.getMessage());
        }
    }

    public void cancelarCadastro() {
        carregarCliente();
    }

    public void cadastrarTelefone() {
        telefone = new Telefone();
        telefone.setTipo("1");
        cadasterTelefone = true;
        visible = false;
        Contexto.getSessao().setAttribute("CADASTROTELEFONE", true);
    }

    public void incluirTelefone() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("sucesso", false);
        String msg = "";
        if (telefone.getId() == null) {
            if (telefone.getTipo() == null || telefone.getTipo().equals("")) {
                addErrorMessages("Tipo de Telefone não Informado!!");
                return;
            }
            if ((telefone.getNmOperadora() == null || telefone.getNmOperadora().equals("")) && telefone.getTipo().equals("1")) {
                addErrorMessages("Operadora não Informado !!");
                return;
            }

            if (telefone.getNumero() == null || telefone.getNumero().equals("")) {
                addErrorMessages("Telefone não Informado !!");
                return;
            }
            String numeroTelefone = telefone.getNumero();
            numeroTelefone = numeroTelefone.replace("(", "");
            numeroTelefone = numeroTelefone.replace(")", "");
            numeroTelefone = numeroTelefone.replace("-", "");
            numeroTelefone = numeroTelefone.trim();
            if (numeroTelefone.length() < (telefone.getTipo().equals("1") ? 11 : 8)) {
                addErrorMessages("Informe um Número Válido !!");
                return;
            }
            cliente.getPessoa().getListTelefone().add(telefone);
            msg = "Telefone Inserido com Sucesso !!";
        } else {
            for (Telefone tel : cliente.getPessoa().getListTelefone()) {
                if (tel.getId().equals(telefone.getId())) {
                    tel = telefone;
                    break;
                }
            }
            msg = "Telfene Atualizado com Sucesso !!";
        }
        Contexto.getSessao().setAttribute("CLIENTE", cliente);
        context.addCallbackParam("sucesso", true);
        cancelarTelefone();

        addSuccessMessage(msg);
    }

    public void cancelarTelefone() {
        Contexto.getSessao().setAttribute("CADASTROTELEFONE", false);
        cadasterTelefone = false;
        telefone = null;
        if (!isEditable) {
            visible = true;
        }
    }

    public void editarCliente() {
        Contexto.getSessao().setAttribute("CADASTROCLIENTE", true);
        Contexto.getSessao().setAttribute("CLIENTE", cliente);
        isEditable = true;
    }

    public void deletarCliente() {
        clienteBean.remove(cliente.getId());
        carregarCliente();
        addSuccessMessage("Registro deletado com Sucesso !!");
    }

    public void editarTelefone() {
        Contexto.getSessao().setAttribute("CADASTROTELEFONE", true);
        Contexto.getSessao().setAttribute("TELEFONE", telefone);
    }

    public void deletarTelefone() {
        if (telefone.getId() != null) {
            clienteBean.remove(telefone.getId());
            cliente.getPessoa().getListTelefone().remove(telefone);
        } else {
            cliente.getPessoa().getListTelefone().remove(telefone);
        }
        addSuccessMessage("Registro de Telefone Excluido com Sucesso !!");
    }

    //**************************************************************************
    public ClienteBean getClienteBean() {
        return clienteBean;
    }

    public void setClienteBean(ClienteBean clienteBean) {
        this.clienteBean = clienteBean;
    }

    public List<Telefone> getTelefones() {
        if (telefones == null) {
            telefones = new ArrayList<>();
        }
        return telefones;
    }

    public void setTelefones(List<Telefone> telefones) {
        this.telefones = telefones;
    }

    /**
     * @return the nmCliente
     */
    public String getNmCliente() {
        return nmCliente;
    }

    /**
     * @param nmCliente the nmCliente to set
     */
    public void setNmCliente(String nmCliente) {
        this.nmCliente = nmCliente;
    }

    /**
     * @return the pessoa
     */
    public Pessoa getPessoa() {
        return pessoa;
    }

    /**
     * @param pessoa the pessoa to set
     */
    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    /**
     * @return the clientes
     */
    public List<Cliente> getClientes() {
        if (clientes == null) {
            clientes = new ArrayList<>();
        }
        return clientes;
    }

    /**
     * @param clientes the clientes to set
     */
    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    /**
     * @return the cadasterTelefone
     */
    public boolean isCadasterTelefone() {
        cadasterTelefone = Contexto.getSessao().getAttribute("CADASTROTELEFONE") == null ? false : (boolean) Contexto.getSessao().getAttribute("CADASTROTELEFONE");
        return cadasterTelefone;
    }

    /**
     * @param cadasterTelefone the cadasterTelefone to set
     */
    public void setCadasterTelefone(boolean cadasterTelefone) {
        this.cadasterTelefone = cadasterTelefone;
    }

    /**
     * @return the endereco
     */
    public Endereco getEndereco() {
        return endereco;
    }

    /**
     * @param endereco the endereco to set
     */
    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    /**
     * @return the telefone
     */
    public Telefone getTelefone() {
        return telefone;
    }

    /**
     * @param telefone the telefone to set
     */
    public void setTelefone(Telefone telefone) {
        this.telefone = telefone;
    }

    /**
     * @return the visible
     */
    public boolean isVisible() {
        return visible;
    }

    /**
     * @param visible the visible to set
     */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public void instancia() {

        cliente = new Cliente();
        cliente.setAtivo(true);
        cliente.setPessoa(new Pessoa());
        cliente.setNmCliente("asdfkjasdfhkajsdhf kasjdfh kajsdhfkajsdhfka");

        isEditable = false;
        visible = true;

        cadasterTelefone = false;

    }

}
