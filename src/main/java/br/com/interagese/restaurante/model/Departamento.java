/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interagese.restaurante.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author bruno
 */
@Entity
@Table(name = "padrao_departamento")
public class Departamento implements Model {

    @Id
    private Integer id;
    private String nmDepartamento;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Secao> listSecao;

    //******************************* Equals && Hashcode ***********************
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Departamento other = (Departamento) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    //******************************* get && setts *****************************
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNmDepartamento() {
        return nmDepartamento;
    }

    public void setNmDepartamento(String nmDepartamento) {
        this.nmDepartamento = nmDepartamento;
    }

    public List<Secao> getListSecao() {
        if (listSecao == null) {
            listSecao = new ArrayList<>();
        }
        return listSecao;
    }

    public void setListSecao(List<Secao> listSecao) {
        this.listSecao = listSecao;
    }

    @Override
    public void setId(Object object) {
        this.id = (Integer) object;
    }

}
