/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interagese.restaurante.view;

import br.com.interagese.restaurante.bean.MesasBean;
import br.com.interagese.restaurante.model.Mesas;
import br.com.interagese.restaurante.utils.InterView;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author bruno
 */
@ViewScoped
@ManagedBean
public class MesasView extends InterView {

    @Inject
    @EJB
    private MesasBean mesasBean;
    private List<Mesas> mesas = new ArrayList<>();
    private boolean cadaster = false;

    public MesasView() {
        this.mesasBean = new MesasBean();
        iniciar();
    }

    //***************************** Methods ************************************
    public void iniciar() {
        cadaster = false;
        carregarMesas();
    }

    public void carregarMesas() {
        mesas = mesasBean.getAll();
    }

    //***************************** get && setts *******************************
    public List<Mesas> getMesas() {
        return mesas;
    }

    public void setMesas(List<Mesas> mesas) {
        this.mesas = mesas;
    }

    public boolean isCadaster() {
        return cadaster;
    }

    public void setCadaster(boolean cadaster) {
        this.cadaster = cadaster;
    }

}
