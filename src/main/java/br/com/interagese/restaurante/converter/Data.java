/*
 * Decimal.java
 *
 * Created on 19 de Novembro de 2007, 17:09
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package br.com.interagese.restaurante.converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Adam
 */
@FacesConverter(value = "data")
public class Data implements Converter {

    public Data() {
    }

    public Object getAsObject(FacesContext arg0, UIComponent arg1, String valorTela) throws ConverterException {
        Date valorRetorno;

        String formato = "dd/MM/yyyy";
        SimpleDateFormat formatter = new SimpleDateFormat(formato);
        try {
            valorRetorno = formatter.parse(valorTela);
            return valorRetorno;
        } catch (ParseException ex) {
            Logger.getLogger(ex.toString());
            return null;
        }

    }

    public String getAsString(FacesContext arg0, UIComponent arg1, Object valorTela) throws ConverterException {

        String formato = "dd/MM/yyyy";
        SimpleDateFormat formatter = new SimpleDateFormat(formato);
        Date data = null;
        try {
            data = (Date) valorTela;
        } catch (Exception e) {
            Logger.getLogger(e.toString());
            return null;
        }

        return formatter.format(data);
    }
}
