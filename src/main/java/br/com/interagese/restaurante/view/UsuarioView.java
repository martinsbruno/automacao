/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interagese.restaurante.view;

import br.com.interagese.restaurante.bean.TabusuBean;
import br.com.interagese.restaurante.model.Tabusu;
import br.com.interagese.restaurante.utils.InterView;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author bruno
 */
@ManagedBean
@ViewScoped
public class UsuarioView extends InterView {

    @Inject
    private TabusuBean tabusuBean;
    private List<Tabusu> listUsuario;
    private boolean cadastroUsuario;

    //****************************** Constructor *******************************
    /**
     * Page initialization methods
     */
    public UsuarioView() {
        this.tabusuBean = new TabusuBean();
        iniciar();
    }

    //******************* Methods of Page Cadaster User ************************
    public void iniciar() {
        cadastroUsuario = false;
        carregarUsuario();
    }

    public void carregarUsuario() {
        getListUsuario().addAll(tabusuBean.getAll());
    }

    //********************************** get && setts **************************
    public TabusuBean getTabusuBean() {
        return tabusuBean;
    }

    public void setTabusuBean(TabusuBean tabusuBean) {
        this.tabusuBean = tabusuBean;
    }

    public List<Tabusu> getListUsuario() {
        return listUsuario;
    }

    public void setListUsuario(List<Tabusu> listUsuario) {
        this.listUsuario = listUsuario;
    }

    public boolean isCadastroUsuario() {
        return cadastroUsuario;
    }

    public void setCadastroUsuario(boolean cadastroUsuario) {
        this.cadastroUsuario = cadastroUsuario;
    }

}
