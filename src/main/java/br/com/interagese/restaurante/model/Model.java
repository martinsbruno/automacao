
package br.com.interagese.restaurante.model;

import java.io.Serializable;


public interface Model extends Serializable{
    
    public Object getId();
    
    public void setId(Object object);
    
}
