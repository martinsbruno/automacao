/*
 * Decimal.java
 *
 * Created on 19 de Novembro de 2007, 17:09
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package br.com.interagese.restaurante.converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author Adam
 */
@FacesConverter(value = "horaG")
public class HoraGregorian implements Converter {

    public HoraGregorian() {
    }

    public Object getAsObject(FacesContext arg0, UIComponent arg1, String valorTela) throws ConverterException {
        Date valorRetorno;

        String formato = "HH:mm";
        SimpleDateFormat formatter = new SimpleDateFormat(formato);
        try {
            valorRetorno = formatter.parse(valorTela);
            XMLGregorianCalendar xml = toXMLGregorianCalendar(valorRetorno);
            return xml;
        } catch (ParseException ex) {
            Logger.getLogger(ex.toString());
            return null;
        }

    }

    private XMLGregorianCalendar toXMLGregorianCalendar(Date date) {
        GregorianCalendar gCalendar = new GregorianCalendar();
        gCalendar.setTime(date);
        XMLGregorianCalendar xmlCalendar = null;
        try {
            xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
        } catch (DatatypeConfigurationException ex) {

        }
        return xmlCalendar;
    }

    @Override
    public String getAsString(FacesContext arg0, UIComponent arg1, Object valorTela) throws ConverterException {
        XMLGregorianCalendar calendar = (XMLGregorianCalendar) valorTela;
        String formato = "HH:mm";
        SimpleDateFormat formatter = new SimpleDateFormat(formato);
        Date data = calendar.toGregorianCalendar().getTime();

        return formatter.format(data);
    }
}
