/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interagese.restaurante.view;

import br.com.interagese.restaurante.bean.FornecedorBean;
import br.com.interagese.restaurante.converter.Contexto;
import br.com.interagese.restaurante.model.Fornecedor;
import br.com.interagese.restaurante.utils.InterView;
import br.com.interagese.exception.InterException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import org.primefaces.context.RequestContext;

/**
 *
 * @author bruno
 */
@ManagedBean
@ViewScoped
public class FornecedorView extends InterView {

    @Inject
    private FornecedorBean fornecedorBean;
    private Fornecedor fornecedor;
    private boolean cadasterFornecedor;
    private List<Fornecedor> fornecedores;

    @Override
    public String getTitulo() {
        iniciar();
        return ""; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void getValidarCampos() throws InterException {

        if (fornecedor != null) {
            if (fornecedor.getNmFornecedor() == null || fornecedor.getNmFornecedor().isEmpty()) {
                throw new InterException("Fornecedor não Informado !!");
            }
            if (isEditable) {
                List<Fornecedor> resp = fornecedorBean.findByQueries("SELECT O FROM Fornecedor o where o.nmFornecedor ='" + fornecedor.getNmFornecedor() + "'");
                if (resp.isEmpty()) {
                    throw new InterException("Fornecedor já cadastrado na base de dados");
                }
            }
        } else {
            throw new InterException("Problemas Técnicos, feche a aplicação e abra novamente !!");
        }
        super.getValidarCampos(); //To change body of generated methods, choose Tools | Templates.
    }

    public void iniciar() {
        Contexto.getSessao().setAttribute("INDEX", 4);
        carregarFornecedor();
    }

    public void carregarFornecedor() {
        fornecedores = fornecedorBean.findByQueries("SELECT o FROM Fornecedor o");
        cadasterFornecedor = false;
        isEditable = false;
        Contexto.getSessao().setAttribute("CADASTROFORNECEDOR", false);
    }

    public void adicionarFornecedore() {
        fornecedor = new Fornecedor();
        setCadasterFornecedor(true);
        isEditable = false;
        Contexto.getSessao().setAttribute("CADASTROFORNECEDOR", true);
    }

    public void incluirFornecedor() {
        try {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("sucesso", false);
            String msg = "";
            getValidarCampos();
            if (fornecedor.getId() == null) {
                fornecedorBean.insert(fornecedor);
                msg = "Fornecedor inserido com sucesso !!";
            } else {
                fornecedorBean.update(fornecedor);
                msg = "Fornecedor atualizado com sucesso !!";
            }

            context.addCallbackParam("sucesso", true);
            addSuccessMessage(msg);
            carregarFornecedor();

        } catch (InterException ie) {
            addErrorMessages(ie.getMessage());
        }
    }

    public void cancelarFornecedor() {
        iniciar();

    }

    public void editarFornecedor() {
        cadasterFornecedor = true;
        isEditable = true;
        Contexto.getSessao().setAttribute("CADASTROFORNECEDOR", true);
    }

    public void deletarFornecedor() {
        fornecedorBean.remove(getFornecedor().getId());
        carregarFornecedor();
        addSuccessMessage("Fornecedor Removido com sucesso !!");
    }

    /**
     * @return the fornecedorBean
     */
    public FornecedorBean getFornecedorBean() {
        return fornecedorBean;
    }

    /**
     * @param fornecedorBean the fornecedorBean to set
     */
    public void setFornecedorBean(FornecedorBean fornecedorBean) {
        this.fornecedorBean = fornecedorBean;
    }

    /**
     * @return the fornecedor
     */
    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    /**
     * @param fornecedor the fornecedor to set
     */
    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    /**
     * @return the cadasterFornecedor
     */
    public boolean isCadasterFornecedor() {
        cadasterFornecedor = Contexto.getSessao().getAttribute("CADASTROFORNECEDOR") == null ? false : (boolean) Contexto.getSessao().getAttribute("CADASTROFORNECEDOR");
        return cadasterFornecedor;
    }

    /**
     * @param cadasterFornecedor the cadasterFornecedor to set
     */
    public void setCadasterFornecedor(boolean cadasterFornecedor) {
        this.cadasterFornecedor = cadasterFornecedor;
    }

    /**
     * @return the fornecedores
     */
    public List<Fornecedor> getFornecedores() {
        if (fornecedores == null) {
            fornecedores = new ArrayList<>();
        }
        return fornecedores;
    }

    /**
     * @param fornecedores the fornecedores to set
     */
    public void setFornecedores(List<Fornecedor> fornecedores) {
        this.fornecedores = fornecedores;
    }

}
