/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interagese.restaurante.view;

import br.com.interagese.restaurante.model.GrupoLanches;
import br.com.interagese.restaurante.model.Mesas;
import br.com.interagese.restaurante.utils.InterView;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author bruno
 */
@ManagedBean
@ViewScoped
public class VendasView extends InterView {
    
    private List<Mesas> resultMesas;
    private Integer qtdMesas = 20;
    private Mesas mesaSelecionada;
    private boolean tablePedido;
    private List<GrupoLanches> resultGrupo;
    
    public VendasView() {
        tablePedido = false;
        carregarQtdMesas(qtdMesas);
    }
    
    public void carregarQtdMesas(Integer qtdMesas) {
        resultMesas = new ArrayList<>();
        for (int i = 1; i <= qtdMesas; i++) {
            Mesas m = new Mesas(i);
            resultMesas.add(m);
        }
    }
    
    public void carregarGrupo(Integer qtdGrupo) {
        resultGrupo = new ArrayList<>();
        for (int i = 1; i <= qtdGrupo; i++) {
            GrupoLanches gl = new GrupoLanches(i);
            gl.setActive((i == 1 ? 0 : 1));
            gl.setResultLanches(carregarQtdLanche(8));
            resultGrupo.add(gl);
        }
        
    }
    
    public List<Mesas> carregarQtdLanche(Integer qtdMesas) {
        resultMesas = new ArrayList<>();
        for (int i = 1; i <= qtdMesas; i++) {
            Mesas m = new Mesas(i);
            resultMesas.add(m);
        }
        return resultMesas;
    }
    
    public void carregarMesaSelecionada() {
        tablePedido = true;
        carregarGrupo(3);
    }
    
    public void cancelarMesa() {
        tablePedido = false;
        carregarQtdMesas(qtdMesas);
    }

    //***************************** get && setts *******************************
    public List<Mesas> getResultMesas() {
        if (resultMesas == null) {
            resultMesas = new ArrayList<>();
        }
        return resultMesas;
    }
    
    public void setResultMesas(List<Mesas> resultMesas) {
        this.resultMesas = resultMesas;
    }
    
    public Mesas getMesaSelecionada() {
        return mesaSelecionada;
    }
    
    public void setMesaSelecionada(Mesas mesaSelecionada) {
        this.mesaSelecionada = mesaSelecionada;
    }
    
    public boolean isTablePedido() {
        return tablePedido;
    }
    
    public void setTablePedido(boolean tablePedido) {
        this.tablePedido = tablePedido;
    }
    
    public Integer getQtdMesas() {
        return qtdMesas;
    }
    
    public void setQtdMesas(Integer qtdMesas) {
        this.qtdMesas = qtdMesas;
    }
    
    public List<GrupoLanches> getResultGrupo() {
        if (resultGrupo == null) {
            resultGrupo = new ArrayList<>();
        }
        return resultGrupo;
    }
    
    public void setResultGrupo(List<GrupoLanches> resultGrupo) {
        this.resultGrupo = resultGrupo;
    }
}
