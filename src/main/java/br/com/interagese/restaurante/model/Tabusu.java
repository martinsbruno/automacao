/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interagese.restaurante.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author bruno
 */
@Entity
@Table(name = "tabusu")
@NamedQueries({
    @NamedQuery(name = "Tabusu.findAll", query = "SELECT t FROM Tabusu t")})
public class Tabusu implements Model {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "codigo")
    private Integer codigo;
    @Size(max = 8)
    @Column(name = "nome")
    private String nome;
    @Size(max = 6)
    @Column(name = "senha")
    private String senha;
    @Column(name = "tipousu")
    private Character tipousu;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sistema")
    private int sistema;
    @Column(name = "codven")
    private Integer codven;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "vlautoriza")
    private Double vlautoriza;
    @Column(name = "rgcodusu")
    private Integer rgcodusu;
    @Size(max = 8)
    @Column(name = "rgusuario")
    private String rgusuario;
    @Column(name = "rgdata")
    @Temporal(TemporalType.DATE)
    private Date rgdata;
    @Size(max = 1)
    @Column(name = "rgevento")
    private String rgevento;
    // @Size(max = 2147483647)
    @Column(name = "template_1")
    private String template1;
    // @Size(max = 2147483647)
    @Column(name = "template_2")
    private String template2;
    // @Size(max = 2147483647)
    @Column(name = "template_3")
    private String template3;
    //@Size(max = 2147483647)
    @Column(name = "template_4")
    private String template4;
    //@Size(max = 2147483647)
    @Column(name = "template_5")
    private String template5;
    @Column(name = "percdescav")
    private Double percdescav;
    @Column(name = "percdesccc")
    private Double percdesccc;
    @Column(name = "percdescap")
    private Double percdescap;
    @Column(name = "percdesccoav")
    private Double percdesccoav;
    @Column(name = "percdescatav")
    private Double percdescatav;
    @Column(name = "percdesccocc")
    private Double percdesccocc;
    @Column(name = "percdescatcc")
    private Double percdescatcc;
    @Column(name = "percdesccoap")
    private Double percdesccoap;
    @Column(name = "percdescatap")
    private Double percdescatap;
    @Column(name = "percdescjur")
    private Double percdescjur;
    @Column(name = "percdescmul")
    private Double percdescmul;
    @Column(name = "percdesctit")
    private Double percdesctit;
    @Column(name = "visualizardebitos")
    private Character visualizardebitos;

    public Tabusu() {
    }

    public Tabusu(Integer codigo) {
        this.codigo = codigo;
    }

    public Tabusu(Integer codigo, int sistema) {
        this.codigo = codigo;
        this.sistema = sistema;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Character getTipousu() {
        return tipousu;
    }

    public void setTipousu(Character tipousu) {
        this.tipousu = tipousu;
    }

    public int getSistema() {
        return sistema;
    }

    public void setSistema(int sistema) {
        this.sistema = sistema;
    }

    public Integer getCodven() {
        return codven;
    }

    public void setCodven(Integer codven) {
        this.codven = codven;
    }

    public Double getVlautoriza() {
        return vlautoriza;
    }

    public void setVlautoriza(Double vlautoriza) {
        this.vlautoriza = vlautoriza;
    }

    public Integer getRgcodusu() {
        return rgcodusu;
    }

    public void setRgcodusu(Integer rgcodusu) {
        this.rgcodusu = rgcodusu;
    }

    public String getRgusuario() {
        return rgusuario;
    }

    public void setRgusuario(String rgusuario) {
        this.rgusuario = rgusuario;
    }

    public Date getRgdata() {
        return rgdata;
    }

    public void setRgdata(Date rgdata) {
        this.rgdata = rgdata;
    }

    public String getRgevento() {
        return rgevento;
    }

    public void setRgevento(String rgevento) {
        this.rgevento = rgevento;
    }

    public String getTemplate1() {
        return template1;
    }

    public void setTemplate1(String template1) {
        this.template1 = template1;
    }

    public String getTemplate2() {
        return template2;
    }

    public void setTemplate2(String template2) {
        this.template2 = template2;
    }

    public String getTemplate3() {
        return template3;
    }

    public void setTemplate3(String template3) {
        this.template3 = template3;
    }

    public String getTemplate4() {
        return template4;
    }

    public void setTemplate4(String template4) {
        this.template4 = template4;
    }

    public String getTemplate5() {
        return template5;
    }

    public void setTemplate5(String template5) {
        this.template5 = template5;
    }

    public Double getPercdescav() {
        return percdescav;
    }

    public void setPercdescav(Double percdescav) {
        this.percdescav = percdescav;
    }

    public Double getPercdesccc() {
        return percdesccc;
    }

    public void setPercdesccc(Double percdesccc) {
        this.percdesccc = percdesccc;
    }

    public Double getPercdescap() {
        return percdescap;
    }

    public void setPercdescap(Double percdescap) {
        this.percdescap = percdescap;
    }

    public Double getPercdesccoav() {
        return percdesccoav;
    }

    public void setPercdesccoav(Double percdesccoav) {
        this.percdesccoav = percdesccoav;
    }

    public Double getPercdescatav() {
        return percdescatav;
    }

    public void setPercdescatav(Double percdescatav) {
        this.percdescatav = percdescatav;
    }

    public Double getPercdesccocc() {
        return percdesccocc;
    }

    public void setPercdesccocc(Double percdesccocc) {
        this.percdesccocc = percdesccocc;
    }

    public Double getPercdescatcc() {
        return percdescatcc;
    }

    public void setPercdescatcc(Double percdescatcc) {
        this.percdescatcc = percdescatcc;
    }

    public Double getPercdesccoap() {
        return percdesccoap;
    }

    public void setPercdesccoap(Double percdesccoap) {
        this.percdesccoap = percdesccoap;
    }

    public Double getPercdescatap() {
        return percdescatap;
    }

    public void setPercdescatap(Double percdescatap) {
        this.percdescatap = percdescatap;
    }

    public Double getPercdescjur() {
        return percdescjur;
    }

    public void setPercdescjur(Double percdescjur) {
        this.percdescjur = percdescjur;
    }

    public Double getPercdescmul() {
        return percdescmul;
    }

    public void setPercdescmul(Double percdescmul) {
        this.percdescmul = percdescmul;
    }

    public Double getPercdesctit() {
        return percdesctit;
    }

    public void setPercdesctit(Double percdesctit) {
        this.percdesctit = percdesctit;
    }

    public Character getVisualizardebitos() {
        return visualizardebitos;
    }

    public void setVisualizardebitos(Character visualizardebitos) {
        this.visualizardebitos = visualizardebitos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigo != null ? codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tabusu)) {
            return false;
        }
        Tabusu other = (Tabusu) object;
        if ((this.codigo == null && other.codigo != null) || (this.codigo != null && !this.codigo.equals(other.codigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.interagese.restaurante.model.Tabusu[ codigo=" + codigo + " ]";
    }

    @Override
    public Object getId() {
        return codigo;
    }

    @Override
    public void setId(Object object) {
        this.codigo = (Integer) object;
    }

}
