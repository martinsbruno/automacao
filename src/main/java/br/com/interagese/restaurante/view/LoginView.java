/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interagese.restaurante.view;

import br.com.interagese.restaurante.converter.Contexto;
import static br.com.interagese.restaurante.utils.InterView.listUsuarioLogado;
import br.com.interagese.exception.InterException;
import br.com.interagese.restaurante.bean.TabusuBean;
import br.com.interagese.restaurante.model.Tabusu;

import java.util.ArrayList;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import org.primefaces.context.RequestContext;

/**
 *
 * @author bruno
 */
@ManagedBean
@SessionScoped
public class LoginView {

    @Inject
    private TabusuBean tabusuBean;
    private String nomeUsuario;
    private String senha;
    private boolean isLogin;

    public LoginView() {
  
    }

    public void getValidarCampos() throws InterException {
        if (getNomeUsuario() == null || getNomeUsuario().isEmpty()) {
            throw new InterException("Nome do Usuário não Informado!!");
        }
        if (getSenha() == null || getSenha().isEmpty()) {
            throw new InterException("Senha não Informada!!");
        }

    }

    public void iniciar() {
        setNomeUsuario("");
        setSenha("");
        isLogin = true;
        Contexto.getSessao().getId();
//        dataAtual = DateFormat.getDateInstance(DateFormat.FULL, new Locale("pt", "BR")).format(new Date());
    }

    public void confirmarUsuario() {
        try {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("sucesso", false);
            context.addCallbackParam("usuariologado", false);
            getValidarCampos();
            Tabusu user = getTabusuBean().login(getNomeUsuario(), getSenha());
            if (user != null) {
                boolean isExist = false;
                if (listUsuarioLogado != null && !listUsuarioLogado.isEmpty()) {
                    for (Tabusu u : listUsuarioLogado) {
                        if (user.getCodigo().equals(u.getCodigo())) {
                            isExist = true;
                            break;
                        }
                    }
                } else {
                    listUsuarioLogado = new ArrayList<>();
                }
                if (!isExist) {
                    listUsuarioLogado.add(user);
                    createSessionUser(user);
                } else {
                    listUsuarioLogado.remove(user);
                    Contexto.getSessao().invalidate();
                    context.addCallbackParam("usuariologado", true);
                    createSessionUser(user);
                    return;
                }

                addSuccessMessage("Usuário Logado com Sucesso!!");
                context.addCallbackParam("sucesso", true);
            } else {
                throw new InterException("Usuário não Localizado na Base de Dados!!");
            }
        } catch (InterException ie) {
            nomeUsuario = "";
            senha = "";
            addErrorMessages(ie.getMessage());

        }
    }

    public void createSessionUser(Tabusu t) {
        Contexto.getSessao().setAttribute("USUARIO", t);
        Contexto.getSessao().setAttribute("INDEX", 1);
    }

    public void criarContaLink() {
        isLogin = false;
        nomeUsuario = "";
        senha = "";
    }

    public void salvarUser() {
        try {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("sucesso", false);
            getValidarCampos();
            System.out.println("tabusu = "+ tabusuBean);
            tabusuBean.salvarUsuario(nomeUsuario, senha);
            iniciar();
            addSuccessMessage("Usuário criado com Sucesso !!");
            context.addCallbackParam("sucesso", true);
        } catch (InterException ie) {
            nomeUsuario = "";
            senha = "";
            addErrorMessages(ie.getMessage());
        }

    } 

    public void addErrorMessages(String mensagem) {
        FacesMessage msg;
        msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, mensagem, "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void addSuccessMessage(String mensagem) {
        FacesMessage msg;
        msg = new FacesMessage(FacesMessage.SEVERITY_INFO, mensagem, "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void addAlertMessage(String mesangem) {
        FacesMessage msg;
        msg = new FacesMessage(FacesMessage.SEVERITY_WARN, mesangem, "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    //***************************** get && setts *******************************
    public TabusuBean getTabusuBean() {
        return tabusuBean;
    }

    public void setTabusuBean(TabusuBean tabusuBean) {
        this.tabusuBean = tabusuBean;
    }

    /**
     * @return the nomeUsuario
     */
    public String getNomeUsuario() {
        return nomeUsuario;
    }

    /**
     * @param nomeUsuario the nomeUsuario to set
     */
    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    /**
     * @return the senha
     */
    public String getSenha() {
        return senha;
    }

    /**
     * @param senha the senha to set
     */
    public void setSenha(String senha) {
        this.senha = senha;
    }

    public boolean isIsLogin() {
        return isLogin;
    }

    public void setIsLogin(boolean isLogin) {
        this.isLogin = isLogin;
    }

}
