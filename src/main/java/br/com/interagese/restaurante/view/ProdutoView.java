/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interagese.restaurante.view;

import br.com.interagese.restaurante.bean.ProdutoBean;
import br.com.interagese.restaurante.converter.Contexto;
import br.com.interagese.restaurante.converter.Utils;
import br.com.interagese.restaurante.model.ComposicaoProduto;
import br.com.interagese.restaurante.model.Departamento;
import br.com.interagese.restaurante.model.Fornecedor;
import br.com.interagese.restaurante.model.HistoricoPrecoProduto;
import br.com.interagese.restaurante.model.Produto;
import br.com.interagese.restaurante.model.Secao;
import br.com.interagese.restaurante.model.Unidade;
import br.com.interagese.restaurante.utils.InterView;
import br.com.interagese.exception.InterException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import org.primefaces.context.RequestContext;

/**
 *
 * @author bruno
 */
@ManagedBean
@ViewScoped
public class ProdutoView extends InterView {
    
    @Inject
    private ProdutoBean produtoBean;
    private Produto produto;
    private boolean cadasterProduto;
    private boolean cadasterHistorico;
    private boolean cadasterComposicao;
    private List<Produto> produtos;
    private ComposicaoProduto composicaoProduto;
    private HistoricoPrecoProduto historicoProduto;
    
    @Override
    public String getTitulo() {
        iniciar();
        return ""; //To change body of generated methods, choose Tools | Templates.
    }
    
    public void iniciar() {
        Contexto.getSessao().setAttribute("INDEX", 5);
        carregarProduto();
    }
    
    public void carregarProduto() {
        produtos = produtoBean.findByQueries("SELECT o FROM Produto o");
        cadasterProduto = false;
        Contexto.getSessao().setAttribute("CADASTROPRODUTO", false);
    }
    
    public void adicionarProduto() {
        produto = new Produto();
        cadasterProduto = true;
        cadasterComposicao = false;
        Contexto.getSessao().setAttribute("CADASTROPRODUTO", true);
        Contexto.getSessao().setAttribute("CADASTROCOMPOSICAO", false);
        Contexto.getSessao().setAttribute("PRODUTO", null);
        isEditable = false;
    }
    
    public void adicionarComposicao() {
        composicaoProduto = new ComposicaoProduto();
        cadasterComposicao = true;
        Contexto.getSessao().setAttribute("CADASTROCOMPOSICAO", true);
        
    }
    
    public void incluirComposicao() {
        try {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("sucesso", false);
            if (composicaoProduto != null) {
                if (composicaoProduto.getProduto() == null) {
                    throw new InterException("Produto não Informado !!");
                }
                if (composicaoProduto.getQuantidade() == null) {
                    throw new InterException("Quantidade não Informada !!");
                }
                if (composicaoProduto.getUnidade() == null) {
                    throw new InterException("Unidade não Informada !!");
                }
                String mensagem = "Composição Produto ";
                if (composicaoProduto.getId() == null) {
                    produto.getListComposicaoProduto().add(composicaoProduto);
                    mensagem += "Adicionado com sucesso !!";
                } else {
                    for (ComposicaoProduto c : produto.getListComposicaoProduto()) {
                        if (c.getId().equals(composicaoProduto.getId())) {
                            c = composicaoProduto;
                            break;
                        }
                    }
                    mensagem += "Atualizado com sucesso !!";
                }
                addSuccessMessage(mensagem);
                context.addCallbackParam("sucesso", false);
            } else {
                throw new InterException("Composição Produto está nullo !!");
            }
        } catch (InterException ie) {
            addErrorMessages(ie.getMessage());
        }
    }
    
    public void cancelarComposicao() {
        cadasterComposicao = false;
        composicaoProduto = null;
        Contexto.getSessao().setAttribute("CADASTROCOMPOSICAO", false);
    }
    
    public void adicionarHistorico() {
        historicoProduto = new HistoricoPrecoProduto();
//        historicoProduto.setPercentualVendaAtacado(0.0);
//        historicoProduto.setPercentualVendaVarejo(0.0);
//        historicoProduto.setPrecoVendaAtacado(0.0);
//        historicoProduto.setPrecoVendaVarejo(0.0);
        cadasterHistorico = true;
        Contexto.getSessao().setAttribute("CADASTROHISTORICO", true);
    }
    
    public void percentualAtacadoOnChange() {
        Double valorCompra = historicoProduto.getPrecoCompra() == null ? 0.0 : historicoProduto.getPrecoCompra();
        Double perct = historicoProduto.getPercentualVendaAtacado() == null ? 0.0 : historicoProduto.getPercentualVendaAtacado();
        historicoProduto.setPrecoVendaAtacado(Utils.arredondamento((valorCompra * (perct / 100)) + valorCompra));
    }

    public void percentualvarejoOnChange() {
         Double valorCompra = historicoProduto.getPrecoCompra() == null ? 0.0 : historicoProduto.getPrecoCompra();
        Double perct = historicoProduto.getPercentualVendaVarejo() == null ? 0.0 : historicoProduto.getPercentualVendaVarejo();
        historicoProduto.setPrecoVendaVarejo(Utils.arredondamento((valorCompra * (perct / 100)) + valorCompra));
    }
    
    public void incluirHistorico() {
        try {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("sucesso", false);
            if (historicoProduto != null) {
                if (historicoProduto.getDataRegistro() == null) {
                    throw new InterException("Data Registro não Informada !!");
                }
                if (historicoProduto.getPrecoCompra() == null) {
                    throw new InterException("Preço compra não Informado !!");
                }
                if (historicoProduto.getPercentualVendaVarejo() == null && historicoProduto.getPrecoVendaVarejo() == null) {
                    throw new InterException("Preço Varejo não Informado !!");
                }
                String mensagem = "Historico Produto ";
                if (historicoProduto.getId() == null) {
                    produto.getListHistoricoPrecoProduto().add(historicoProduto);
                    mensagem += "Adicionado com Sucesso !!";
                } else {
                    for (HistoricoPrecoProduto h : produto.getListHistoricoPrecoProduto()) {
                        if (h.getId().equals(historicoProduto.getId())) {
                            h = historicoProduto;
                            break;
                        }
                    }
                    mensagem += "Atualizado com Sucesso !!";
                }
                addSuccessMessage(mensagem);
                context.addCallbackParam("sucesso", true);
            } else {
                throw new InterException("Histórico Produto está nullo !!");
            }
        } catch (InterException ie) {
            addErrorMessages(ie.getMessage());
        }
    }
    
    public void cancelarHistorico() {
        historicoProduto = null;
        cadasterHistorico = false;
        Contexto.getSessao().setAttribute("CADASTROHISTORICO", false);
    }
    
    public void cancelarProduto() {
        iniciar();
    }

    //****************************** Select Itens ******************************
    public List<SelectItem> getFornecedores() {
        List<SelectItem> result = new ArrayList<>();
        List<Fornecedor> fornecedores = produtoBean.findByQueries("SELECT o FROM Fornecedor o");
        if (!fornecedores.isEmpty()) {
            for (Fornecedor forne : fornecedores) {
                SelectItem item = new SelectItem();
                item.setLabel(forne.getNmFornecedor());
                item.setValue(forne);
                result.add(item);
            }
        }
        return result;
    }
    
    public List<SelectItem> getUnidades() {
        List<SelectItem> result = new ArrayList<>();
        List<Unidade> unidades = produtoBean.findByQueries("SELECT o FROM Unidade o");
        if (!unidades.isEmpty()) {
            for (Unidade unidade : unidades) {
                SelectItem item = new SelectItem();
                item.setLabel(unidade.getSigla() + "-" + unidade.getNmUnidade());
                item.setValue(unidade);
                result.add(item);
            }
        }
        return result;
    }
    
    public List<SelectItem> getDepartamentos() {
        List<SelectItem> result = new ArrayList<>();
        List<Departamento> departamentos = produtoBean.findByQueries("SELECT o FROM Departamento o");
        if (!departamentos.isEmpty()) {
            for (Departamento depart : departamentos) {
                SelectItem item = new SelectItem();
                item.setLabel(depart.getNmDepartamento());
                item.setValue(depart);
                result.add(item);
            }
        }
        return result;
    }
    
    public List<SelectItem> getSecoes() {
        List<SelectItem> result = new ArrayList<>();
        if (produto.getDepartamento() != null && !produto.getDepartamento().getListSecao().isEmpty()) {
            for (Secao secao : produto.getDepartamento().getListSecao()) {
                SelectItem item = new SelectItem();
                item.setLabel(secao.getNmSecao());
                item.setValue(secao);
                result.add(item);
            }
        }
        return result;
    }

    //****************************** get && setts ******************************
    public ProdutoBean getProdutoBean() {
        return produtoBean;
    }
    
    public void setProdutoBean(ProdutoBean produtoBean) {
        this.produtoBean = produtoBean;
    }
    
    public Produto getProduto() {
        return produto;
    }
    
    public void setProduto(Produto produto) {
        this.produto = produto;
    }
    
    public boolean isCadasterProduto() {
        cadasterProduto = Contexto.getSessao().getAttribute("CADASTROPRODUTO") == null ? false : (boolean) Contexto.getSessao().getAttribute("CADASTROPRODUTO");
        if (cadasterProduto) {
            produto = Contexto.getSessao().getAttribute("PRODUTO") == null ? new Produto() : (Produto) Contexto.getSessao().getAttribute("PRODUTO");
            cadasterComposicao = Contexto.getSessao().getAttribute("CADASTROCOMPOSICAO") == null ? false : (boolean) Contexto.getSessao().getAttribute("CADASTROCOMPOSICAO");
            composicaoProduto = composicaoProduto == null ? new ComposicaoProduto() : composicaoProduto;
            cadasterHistorico = Contexto.getSessao().getAttribute("CADASTROHISTORICO") == null ? false : (boolean) Contexto.getSessao().getAttribute("CADASTROHISTORICO");
            historicoProduto = historicoProduto == null ? new HistoricoPrecoProduto() : historicoProduto;
        }
        System.out.println("cadastro="+cadasterProduto);
        return cadasterProduto;
    }
    
    public void setCadasterProduto(boolean cadasterProduto) {
        this.cadasterProduto = cadasterProduto;
    }
    
    public List<Produto> getProdutos() {
        return produtos;
    }
    
    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }
    
    public boolean isCadasterHistorico() {
        cadasterHistorico = Contexto.getSessao().getAttribute("CADASTROHISTORICO") == null ? false : (boolean) Contexto.getSessao().getAttribute("CADASTROHISTORICO");
        return cadasterHistorico;
    }
    
    public void setCadasterHistorico(boolean cadasterHistorico) {
        this.cadasterHistorico = cadasterHistorico;
    }
    
    public boolean isCadasterComposicao() {
        cadasterComposicao = Contexto.getSessao().getAttribute("CADASTROCOMPOSICAO") == null ? false : (boolean) Contexto.getSessao().getAttribute("CADASTROCOMPOSICAO");
        return cadasterComposicao;
    }
    
    public void setCadasterComposicao(boolean cadasterComposicao) {
        this.cadasterComposicao = cadasterComposicao;
    }
    
    public ComposicaoProduto getComposicaoProduto() {
        return composicaoProduto;
    }
    
    public void setComposicaoProduto(ComposicaoProduto composicaoProduto) {
        this.composicaoProduto = composicaoProduto;
    }
    
    public HistoricoPrecoProduto getHistoricoProduto() {
        return historicoProduto;
    }
    
    public void setHistoricoProduto(HistoricoPrecoProduto historicoProduto) {
        this.historicoProduto = historicoProduto;
    }
    
}
