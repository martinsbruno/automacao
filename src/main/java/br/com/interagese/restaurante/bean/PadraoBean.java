/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interagese.restaurante.bean;

import br.com.interagese.restaurante.converter.Contexto;
import br.com.interagese.restaurante.model.Model;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.persistence.CascadeType;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NoResultException;
import javax.persistence.OneToMany;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author bruno-martins
 * @param <T>
 */
public class PadraoBean<T extends Model> implements IPadraoBean<T> {

    @PersistenceContext
    @Inject
    protected EntityManager em;
    private Class<T> beanClass;
    private Integer maxRowCount = 1000;

    //******************************* EM ***************************************
    public EntityManager getEntityManager() {
        return em;
    }

    @Override
    public Object instanceObject() {
        try {
            beanClass.newInstance();
        } catch (InstantiationException ex) {
            Logger.getLogger(PadraoBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(PadraoBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return beanClass;
    }

    //**************************************************************************
    class AgrupamentoObjetos {

        public Class clazz;
        public List lista = new ArrayList();

        @Override
        public int hashCode() {
            int hash = 7;
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final AgrupamentoObjetos other = (AgrupamentoObjetos) obj;
            if (!Objects.equals(this.clazz, other.clazz)) {
                return false;
            }
            return true;
        }

    }
    //********************************* METHODS ********************************

    public T load(Object id) {
        return getEntityManager().find(beanClass, id);
    }

    public synchronized Integer getCodMax() {

        TypedQuery<Integer> query = getEntityManager()
                .createQuery("SELECT MAX(o.id) FROM " + beanClass.getName() + " o ", Integer.class);

        if (query.getSingleResult() != null) {
            return query.getSingleResult() + 1;
        }

        return 1;

    }

    public Integer count() {
        return getEntityManager().createQuery("SELECT COUNT(bc) FROM " + beanClass.getName() + " bc", Long.class)
                .getSingleResult().intValue();
    }

    public List<T> getAll() {
        List<T> result = new ArrayList<>();
        try {
            int countMax = ((Number) count()).intValue();
            int diference = countMax / maxRowCount;
            EntityManager em = getEntityManager();

            if (countMax % maxRowCount != 0) {
                diference++;
            }
            for (int i = 0; i < diference; i++) {
                result.addAll(em.createNativeQuery("SELECT * FROM " + beanClass.getName(), beanClass).setFirstResult(i * 1000).setMaxResults(1000).getResultList());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public void insert(T bean) {
        /*
		 * Verifica se a chave primaria do objeto é nula Se for, gera o código máximo
         */

        if (bean.getId() == null) {
            bean.setId(getCodMax());
        }
        getEntityManager().persist(bean);
    }

    public T update(T bean) {
        return getEntityManager().merge(bean);
    }

    public void remove(Object id) {
        T entity = this.load(id);
        getEntityManager().remove(entity);
    }

    public void refreshModel(Object o) {
        try {
            em.refresh(o);
        } catch (Exception e) {
        }
    }

    public List findByQueries(String consulta) {
        return findByQueries(consulta, 0, 0);
    }

    public Object findByPrimaryKey(Object obj, String condicao) {
        Field[] field = beanClass.getDeclaredFields();
        Field fieldChavePrimaria = null;

        for (Field f : field) {
            if (f.isAnnotationPresent(Id.class)) {
                fieldChavePrimaria = f;
                break;
            }
        }

        String consulta = "select o from " + beanClass.getName() + " o where o."
                + fieldChavePrimaria.getName() + " = :valor " + condicao;
        try {
            Object o = getEntityManager().createQuery(consulta).setParameter("valor", obj).getSingleResult();
            return o;
        } catch (Exception e) {
            return null;
        }
    }

    public Object findByDescricao(String valorDesc, String campoDesc, String condicao) {
        try {

            String consulta = "select o from " + beanClass.getName() + " o where o." + campoDesc + " like '%" + valorDesc + "%'  "
                    + " " + condicao + "";

            List result = getEntityManager().createQuery(consulta).getResultList();

            if (result.isEmpty()) {
                return null;
            }

            if (result.size() > 1) {
                return null;
            } else {
                return result.get(0);
            }

        } catch (Exception e) {
            return null;
        }
    }

    private Field getFieldId(Class clazz) {

        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(Id.class)) {
                return field;
            }
        }
        return null;
    }

    public void addErro(String mensagem) {
        try {
            em.getTransaction().rollback();
        } catch (Exception e) {
        }

    }

    public List findByQueries(String consulta, int quantRegistros, int posicao) {
        if (quantRegistros == 0 && posicao == 0) {
            return em.createQuery(consulta).
                    getResultList();
        } else {
            return em.createQuery(consulta).
                    setMaxResults(quantRegistros).
                    setFirstResult(posicao).
                    getResultList();
        }
    }

    public Long getCountResult(String consulta) {
        int iFrom = consulta.toUpperCase().indexOf("FROM");
        String sResult = "select DISTINCT COUNT(o) " + consulta.substring(iFrom);
        Long quant;
        try {
            quant = (Long) em.createQuery(sResult).getSingleResult();
        } catch (NoResultException ex) {
            quant = 0L;
        }
        return quant;
    }

    private boolean isCascadeAll(OneToMany oneToMany) {

        CascadeType[] cascadeTypes = oneToMany.cascade();
        boolean isCascadeAll = false;
        for (CascadeType cascadeType : cascadeTypes) {
            if (cascadeType.equals(CascadeType.ALL)) {
                isCascadeAll = true;
                break;
            }
        }

        return isCascadeAll;
    }

    private boolean isCascadeAll(ManyToOne manyToOne) {

        CascadeType[] cascadeTypes = manyToOne.cascade();
        boolean isCascadeAll = false;
        for (CascadeType cascadeType : cascadeTypes) {
            if (cascadeType.equals(CascadeType.ALL)) {
                isCascadeAll = true;
                break;
            }
        }

        return isCascadeAll;
    }

    public String getIp() {
        HttpServletRequest rq = (HttpServletRequest) Contexto.getExternalContext().getRequest();

        String ipAddress = rq.getRemoteAddr();

        if (ipAddress == null) {
            ipAddress = "127.0.0.1";
        }

        if (ipAddress.contains("0:0:0:")) {
            ipAddress = "127.0.0.1";
        }

        return ipAddress;
    }

    public String getIpFisico() {
        String ipAddress = null;
        Enumeration<NetworkInterface> net = null;
        try {
            net = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }

        while (net.hasMoreElements()) {
            NetworkInterface element = net.nextElement();
            Enumeration<InetAddress> addresses = element.getInetAddresses();
            while (addresses.hasMoreElements()) {
                InetAddress ip = addresses.nextElement();

                if (ip.isSiteLocalAddress()) {
                    ipAddress = ip.getHostAddress();
                }
            }
        }
        return ipAddress;
    }

    public String getIpRelativo() {
        String ip = "";
        InetAddress addr;
        try {
            addr = InetAddress.getLocalHost();
            ip = addr.getHostAddress();
        } catch (UnknownHostException ex) {
            Logger.getLogger(PadraoBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ip;
    }

    public String getnmMaquinaIpReal() {
        String nmMaquinaIpReal = "";
        try {
            InetAddress myself = InetAddress.getLocalHost();
            nmMaquinaIpReal = myself.getHostName();
            //ipReal = myself.getHostAddress();
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
        return nmMaquinaIpReal;
    }
}
