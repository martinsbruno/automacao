/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interagese.restaurante.utils;

import br.com.interagese.restaurante.model.Usuario;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author bruno
 */
@ManagedBean
@SessionScoped
public abstract class PadraoView {

    public Integer index;
    public Usuario usuarioLogado;

    public abstract String getTitulo();

    public Usuario getUsuario() {
        return usuarioLogado;
    }
    
    public String getSaudacao() {
        String result = "Em, " + new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + " "
                + "        " + " - Seja bem-vindo,  " + getUsuario().getNmUsuario().toUpperCase();
        return result;
    }

}
