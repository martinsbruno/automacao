/*
 * Contexto.java
 *
 * Created on 12 de Junho de 2007, 15:59
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package br.com.interagese.restaurante.converter;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Contexto {

    /**
     * Creates a new instance of Contexto
     */
    private static List mensagemErro = new ArrayList();

    public Contexto() {
    }

    public static HttpSession getSessao() {
        return (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
    }

    public static HttpServletResponse getResponse() {
        return (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
    }

    public static HttpServletRequest getRequest() {
        return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    }

    public static ExternalContext getExternalContext() {
        return FacesContext.getCurrentInstance().getExternalContext();
    }

    public static String getPath(String caminho) {
        ServletContext request = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();

        return request.getRealPath(caminho);

    }

    public static List getMensagemErro() {
        return mensagemErro;
    }

    public static void addMensagemErro(String MensagemErro) {
        mensagemErro.add(MensagemErro);
    }

    public static void clearMensagemErro() {
        mensagemErro.clear();
    }

    public static String getDatetoString(Date data) {
        if (data == null) {
            return null;
        }

        String sTemp = DateFormat.getDateInstance().format(data);
        //     dd/mm/yyyy
        String dataFinal = sTemp.substring(6);
        dataFinal = dataFinal + "/";
        dataFinal = dataFinal + sTemp.substring(3, 5);
        dataFinal = dataFinal + "/";
        dataFinal = dataFinal + sTemp.substring(0, 2);

        return dataFinal;

    }

    public static String getDateStringtoString(String data) {
        if (data.equals("")) {
            return null;
        }

        String sTemp = data;

        String dataFinal = sTemp.substring(6);
        dataFinal = dataFinal + "/";
        dataFinal = dataFinal + sTemp.substring(3, 5);
        dataFinal = dataFinal + "/";
        dataFinal = dataFinal + sTemp.substring(0, 2);

        return dataFinal;

    }
}
