/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interagese.restaurante.bean;

import br.com.interagese.exception.InterException;
import br.com.interagese.restaurante.model.Tabusu;
import java.util.Date;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

/**
 *
 * @author bruno
 */
@Stateless
@LocalBean
public class TabusuBean extends PadraoBean<Tabusu> {

    public Tabusu login(String nmUsuario, String senha) {
        TypedQuery<Tabusu> query = em.createQuery("SELECT o FROM Tabusu o WHERE o.nome = :nome and o.senha = :senha ", Tabusu.class);
        query.setParameter("nome", nmUsuario.toUpperCase());
        query.setParameter("senha", senha);

        return query.getResultList().isEmpty() ? null : query.getResultList().get(0);
    }

    public void salvarUsuario(String nmUsuario, String senha) throws InterException {
        Tabusu u = login(nmUsuario, senha);
        if (u != null) {
            throw new InterException("Usuário já cadastrado no Banco de Dados !!");
        } else {
            u = new Tabusu(getCodMax(), 6);
            u.setNome(nmUsuario.toUpperCase());
            u.setSenha(senha);
            u.setRgdata(new Date());
            u.setTipousu(("A").charAt(0));
            u.setRgusuario("RESTAURANTE");
            u.setRgevento("1");
        }
        super.insert(u);
    }

}
