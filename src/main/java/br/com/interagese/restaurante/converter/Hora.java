/*
 * Decimal.java
 *
 * Created on 19 de Novembro de 2007, 17:09
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package br.com.interagese.restaurante.converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Adam
 */
@FacesConverter(value = "hora")
public class Hora implements Converter {

    public Hora() {
    }

    @Override
    public Object getAsObject(FacesContext arg0, UIComponent arg1, String valorTela) throws ConverterException {
        Date valorRetorno;
        if (valorTela == null || valorTela.trim().equals("")) {
            return null;
        }

        String formato = "HH:mm";
        SimpleDateFormat formatter = new SimpleDateFormat(formato);
        try {
            valorRetorno = formatter.parse(valorTela);
            return valorRetorno;
        } catch (ParseException ex) {
            ex.printStackTrace();
            return null;
        }

    }

    @Override
    public String getAsString(FacesContext arg0, UIComponent arg1, Object valorTela) throws ConverterException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

        return dateFormat.format(valorTela);
    }
}
