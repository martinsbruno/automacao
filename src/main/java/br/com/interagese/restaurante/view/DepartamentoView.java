/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interagese.restaurante.view;

import br.com.interagese.restaurante.bean.DepartamentoBean;
import br.com.interagese.restaurante.converter.Contexto;
import br.com.interagese.restaurante.model.Departamento;
import br.com.interagese.restaurante.utils.InterView;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author bruno
 */
@ManagedBean
@ViewScoped
public class DepartamentoView extends InterView {

    @Inject
    private DepartamentoBean departamentoBean;
    private Departamento departamento;
    private boolean cadasterDepartamento;
    private List<Departamento> departamentos;

    @Override
    public String getTitulo() {
        iniciar();
        return ""; //To change body of generated methods, choose Tools | Templates.
    }

    public void iniciar() {
        Contexto.getSessao().setAttribute("INDEX", 6);
        carregarDepartamento();
    }

    public void carregarDepartamento() {
        departamentos = departamentoBean.findByQueries("SELECT o FROM Departamento o");
        cadasterDepartamento = false;
        Contexto.getSessao().setAttribute("CADASTRODEPARTAMENTO", false);
    }

    //****************************** get && setts ******************************
    /**
     * @return the departamentoBean
     */
    public DepartamentoBean getDepartamentoBean() {
        return departamentoBean;
    }

    /**
     * @param departamentoBean the departamentoBean to set
     */
    public void setDepartamentoBean(DepartamentoBean departamentoBean) {
        this.departamentoBean = departamentoBean;
    }

    /**
     * @return the departamento
     */
    public Departamento getDepartamento() {
        return departamento;
    }

    /**
     * @param departamento the departamento to set
     */
    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    /**
     * @return the cadasterDepartamento
     */
    public boolean isCadasterDepartamento() {
        cadasterDepartamento=Contexto.getSessao().getAttribute("CADASTRODEPARTAMENTO") == null ? false : (boolean) Contexto.getSessao().getAttribute("CADASTRODEPARTAMENTO");
        return cadasterDepartamento;
    }

    /**
     * @param cadasterDepartamento the cadasterDepartamento to set
     */
    public void setCadasterDepartamento(boolean cadasterDepartamento) {
        this.cadasterDepartamento = cadasterDepartamento;
    }

    /**
     * @return the departamentos
     */
    public List<Departamento> getDepartamentos() {
        if (departamentos == null) {
            departamentos = new ArrayList<>();
        }
        return departamentos;
    }

    /**
     * @param departamentos the departamentos to set
     */
    public void setDepartamentos(List<Departamento> departamentos) {
        this.departamentos = departamentos;
    }

}
