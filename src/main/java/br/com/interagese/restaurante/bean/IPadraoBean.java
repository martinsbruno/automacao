/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interagese.restaurante.bean;

import br.com.interagese.restaurante.model.Model;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author bruno-martins
 * @param <T>
 */
public interface IPadraoBean<T extends Model> extends Serializable {

    public List findByQueries(String consulta, int quantRegistros, int posicao);

    public Object findByPrimaryKey(Object obj, String condicao);

    public Object instanceObject();

    public Long getCountResult(String consulta);

    public Object findByDescricao(String valorDesc, String campoDesc, String condicao);

    public Integer getCodMax();

    public T load(Object id);

    public List<T> getAll();

    public void insert(T bean);

    public T update(T bean);

    public void remove(Object id);

}
