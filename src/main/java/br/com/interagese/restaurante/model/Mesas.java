/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interagese.restaurante.model;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author bruno
 */
@Entity
@Table(name = "padrao_mesas")
public class Mesas implements Model {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nmMesa;
    private String nmLanche;

    public Mesas() {
    }
    //**************************** Equals && Hashcode **************************

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Mesas other = (Mesas) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    

    //***************************** contructor *********************************
    public Mesas(Integer id) {
        this.id = id;
    }

    //***************************** get && setts *******************************
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNmMesa() {
        nmMesa = "Mesa " + id;
        return nmMesa;
    }

    public String getNmLanche() {
        nmLanche = "Lanche " + id;
        return nmLanche;
    }

    @Override
    public void setId(Object object) {
        this.id = (Integer) object;
    }

}
